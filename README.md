This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install

### `npm install` `yarn install`

## Available Scripts

In the project directory, you can run:

### `npm start` `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


## ToDo

As a team, create a flower market.

Tasks need to be done :

#### FLOWERMARKET-1 (Render and Style)
1. Render table of flowers
2. Make it looks cool.

#### FLOWERMARKET-2 (Add new Flower)

Implement the form for adding a new flower to the list.

#### FLOWERMARKET-3 (Delete Flower)

Add a button that will allow us to remove the flower from the list, remember about confirmation modal.

#### FLOWERMARKET-4 (Update Existing Flower)

Implement the form for update the flower on the list.

#### FLOWERMARKET-5 (Color Flower Row)

Color text according to stock
  - stock >= 10 (green)
  - 10 > stock >= 3 (yellow)
  - 3 > stock (red)

#### FLOWERMARKET-6 (Sort Flowers)

1. Flowers should be sorted according to stock or price
2. There should be possibility to change sorting stock <--> price

#### FLOWERMARKET-7 (Add Flower Product Modal)

1. Add description and Image(can be random from internet) 
2. On click on Flower - Modal should appear
3. On modal we should see Flower Name/Image/Description/Price/Stock
4. User should be able to dismiss modal


#### FLOWERMARKET-8 (Add Something Extra)

Talk with team and decide what should you implement to make us happy