import React, { Component } from 'react';
import './App.scss';
import flowers from './flowers.json'



class Form extends Component {


  render() {

    return (
      <form onSubmit={this.props.handleSubmit} className="form">
        <label htmlFor="flowername">
          Fill input:
          <input type="text" id="flowername" placeholder="Enter your flower"
            value={this.props.value}
            onChange={this.props.handleChange}
          />
        </label>

        <label htmlFor="flowerstock">
          Stock:
          <input type="number" id="flowerstock" placeholder="Enter stock"
            value={this.props.stockValue}
            onChange={this.props.handleChangeStock}
          />
        </label>

        <label htmlFor="flowerprice">
          Price:
          <input type="number" id="flowerprice" placeholder="Enter price"
            value={this.props.priceValue}
            onChange={this.props.handleChangePrice}
          />
        </label>
        <button >Add flowers</button>
      </form >
    )
  }
}



class App extends Component {
  constructor() {
    super()
    this.state = {
      flowersList: flowers.list,
      value: '',
      stockValue: '',
      priceValue: '',

    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    console.log(value)
    this.setState({
      value: value,
    })

  }

  handleChangeStock = (e) => {
    const stockValue = e.target.value;
    console.log(stockValue)
    this.setState({
      stockValue: stockValue,
    })

  }

  handleChangePrice = (e) => {
    const priceValue = e.target.value;
    console.log(priceValue)
    this.setState({
      priceValue: priceValue,
    })

  }


  handleSubmit = (e) => {
    e.preventDefault();

    let newFlowerList = this.state.flowersList.slice();
    newFlowerList.push({
      "price": this.state.priceValue,
      "name": this.state.value,
      "id": this.state.flowersList[this.state.flowersList.length - 1].id + 1,
      "stock": this.state.stockValue,
    })

    this.setState({
      value: '',
      flowersList: newFlowerList
    })

    // console.log(this.state.flowersList, this.state.flowersList.length)

  }


  render() {
    const { flowersList } = this.state;
    return (
      <div className="App">
        {flowersList.map(flower => {
          return (
            <div key={flower.id}>
              {flower.name}

            </div>
          )
        })}
        <Form handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
          value={this.state.value}
          handleChangeStock={this.handleChangeStock}
          stockValue={this.state.stockValue}
          handleChangePrice={this.handleChangePrice}
          priceValue={this.state.priceValue}
        />
      </div>
    );
  }
}

export default App;
